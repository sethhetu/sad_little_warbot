//A reminder on keycodes:
// "AXB" = "A action, X amount, B object".
// actions:
//    W : wait X ticks
//    D : set delay after a single spawn to X
//    S : spawn X objects
//
// objects:
//    D : ducklings
//    M : mother ducklings
//    P : plant
//    S : superhero
//    G : purple (globe) hero
//    B : beam
//
// the room is "done" if no more comps are left, 
//   and all enemies are dead (enemy count increases on spawn)

//A few ducks, then 1 mother at the end.
if (global.round_id==1) {
  with (instance_create(obj_toy_spawn_3.x, obj_toy_spawn_3.y, obj_enemy_spawner)) {
    self.comp_stream = "W20:D100:S5D:W200:D60:S5D:";
  }
  
  with (instance_create(obj_toy_spawn_4.x, obj_toy_spawn_4.y, obj_enemy_spawner)) {
    self.comp_stream = "W100:D60:S5D:W200:D30:S10D:W200:W50:S1M:";
  }
} 


//Mix of ducks and mother ducks.
if (global.round_id==2) {
  //Flank the sides with ducks.
  with (instance_create(obj_toy_spawn_1.x, obj_toy_spawn_1.y, obj_enemy_spawner)) {
    self.comp_stream = "W20:D20:S20D:";
  }
  with (instance_create(obj_toy_spawn_5.x, obj_toy_spawn_5.y, obj_enemy_spawner)) {
    self.comp_stream = "W20:D20:S20D:";
  }
  
  //Mother ducks in the middle.
  with (instance_create(obj_toy_spawn_2.x, obj_toy_spawn_2.y, obj_enemy_spawner)) {
    self.comp_stream = "W20:D20:S5D:S1M:S5D:S1M:S5D:S1M:";
  }
  with (instance_create(obj_toy_spawn_3.x, obj_toy_spawn_3.y, obj_enemy_spawner)) {
    self.comp_stream = "W60:D20:S1M:S5D:S1M:S5D:S1M:S5D:S1M:";
  }
  with (instance_create(obj_toy_spawn_4.x, obj_toy_spawn_4.y, obj_enemy_spawner)) {
    self.comp_stream = "W20:D20:S5D:S1M:S5D:S1M:S5D:S1M:";
  }
}


//A plant at the start. Then, ducks and the occasional plant.
if (global.round_id==3) {
  //This one spawns early.
  with (instance_create(obj_garden_spawn_3.x, obj_garden_spawn_3.y, obj_enemy_spawner)) {
    self.comp_stream = "W100:S1P:W550:S1P:W100:S1P:";
  }
  
  //The rest go at later points
  with (instance_create(obj_garden_spawn_2.x, obj_garden_spawn_2.y, obj_enemy_spawner)) {
    self.comp_stream = "W450:S1P:W100:S1P:W100:S1P:";
  }
  with (instance_create(obj_garden_spawn_4.x, obj_garden_spawn_4.y, obj_enemy_spawner)) {
    self.comp_stream = "W550:S1P:W100:S1P:W100:S1P:";
  }
  
  //Ducks keep you away from the edge
  with (instance_create(obj_toy_spawn_1.x, obj_toy_spawn_1.y, obj_enemy_spawner)) {
    self.comp_stream = "W300:D20:S6D:W5:S6D:W5:S6D:W5:S2M:W5:S6D:W5:S6D:W5:S6D:";
  }
  with (instance_create(obj_toy_spawn_3.x, obj_toy_spawn_3.y, obj_enemy_spawner)) {
    self.comp_stream = "W350:D20:S6D:W5:S6D:W5:S6D:W5:S6D:W5:S2M:W5:S6D:W5:S6D:";
  }
  with (instance_create(obj_toy_spawn_5.x, obj_toy_spawn_5.y, obj_enemy_spawner)) {
    self.comp_stream = "W300:D20:S6D:W5:S6D:W5:S6D:W5:S2M:W5:S6D:W5:S6D:W5:S6D:";
  }
  
  //These ducks mess you up near the end round of plants.
  with (instance_create(obj_toy_spawn_2.x, obj_toy_spawn_2.y, obj_enemy_spawner)) {
    self.comp_stream = "W680:D20:S6D:W5:S6D:W5:S6D:";
  }
  with (instance_create(obj_toy_spawn_4.x, obj_toy_spawn_4.y, obj_enemy_spawner)) {
    self.comp_stream = "W720:D20:S6D:W5:S6D:W5:S6D:";
  }
}


//Plants in patterns.
if (global.round_id==4) {
  with (instance_create(obj_garden_spawn_1.x, obj_garden_spawn_1.y, obj_enemy_spawner)) {
    self.comp_stream = "W50:S1P:W150:S1P:W150:S1P:W100:S1P:W200:D30:S3P:";
  }
  with (instance_create(obj_garden_spawn_2.x, obj_garden_spawn_2.y, obj_enemy_spawner)) {
    self.comp_stream = "W52:S1P:W150:S1P:W150:S1P:W100:S1P:W200:D30:S3P:";
  }
  with (instance_create(obj_garden_spawn_3.x, obj_garden_spawn_3.y, obj_enemy_spawner)) {
    self.comp_stream = "W53:S1P:W150:S1P:W150:S1P:W100:S1P:W200:D30:S3P:";
  }
  with (instance_create(obj_garden_spawn_4.x, obj_garden_spawn_4.y, obj_enemy_spawner)) {
    self.comp_stream = "W51:S1P:W150:S1P:W150:S1P:W100:S1P:W200:D30:S3P:";
  }
  with (instance_create(obj_garden_spawn_5.x, obj_garden_spawn_5.y, obj_enemy_spawner)) {
    self.comp_stream = "W49:S1P:W150:S1P:W150:S1P:W100:S1P:W200:D30:S3P:";
  }
}



//One stream of laser superheroes, one stream of globe superheroes.
if (global.round_id==5) {
  with (instance_create(obj_comic_spawn_3.x, obj_comic_spawn_3.y, obj_enemy_spawner)) {
    self.comp_stream = "W5:S1S:W150:S1G:W100:W125:D50:S5S:";
  }
  with (instance_create(obj_comic_spawn_2.x, obj_comic_spawn_2.y, obj_enemy_spawner)) {
    self.comp_stream = "W55:S1S:W100:W40:S1G:W60:W100:D80:S5G:";
  }
  with (instance_create(obj_comic_spawn_4.x, obj_comic_spawn_4.y, obj_enemy_spawner)) {
    self.comp_stream = "W75:S1S:W80:W80:S1G:W20:W85:D50:S5S:";
  }
}


//Same as 5, but with plants and ducks.
if (global.round_id==6) {
  with (instance_create(obj_comic_spawn_3.x, obj_comic_spawn_3.y, obj_enemy_spawner)) {
    self.comp_stream = "W5:S1S:W150:S1G:W100:W125:D50:S5S:";
  }
  with (instance_create(obj_comic_spawn_2.x, obj_comic_spawn_2.y, obj_enemy_spawner)) {
    self.comp_stream = "W55:S1S:W100:W40:S1G:W60:W100:D80:S5G:";
  }
  with (instance_create(obj_comic_spawn_4.x, obj_comic_spawn_4.y, obj_enemy_spawner)) {
    self.comp_stream = "W75:S1S:W80:W80:S1G:W20:W85:D50:S5S:";
  }
  
  //Ducks keep you away from the edge
  with (instance_create(obj_toy_spawn_1.x, obj_toy_spawn_1.y, obj_enemy_spawner)) {
    self.comp_stream = "W300:D20:S6D:W5:S6D:W5:S6D:W5:S2M:W5:S6D:";
  }
  with (instance_create(obj_toy_spawn_3.x, obj_toy_spawn_3.y, obj_enemy_spawner)) {
    self.comp_stream = "W350:D20:S6D:W5:S6D:W5:S6D:W5:S6D:W5:S2M:";
  }
  with (instance_create(obj_toy_spawn_5.x, obj_toy_spawn_5.y, obj_enemy_spawner)) {
    self.comp_stream = "W300:D20:S6D:W5:S6D:W5:S6D:W5:S2M:W5:S6D:";
  }
  
  //Plants fire in pairs.
  with (instance_create(obj_garden_spawn_2.x, obj_garden_spawn_2.y, obj_enemy_spawner)) {
    self.comp_stream = "W52:S1P:W150:W150:S1P:W100:W200:S1P:";
  }
  with (instance_create(obj_garden_spawn_4.x, obj_garden_spawn_4.y, obj_enemy_spawner)) {
    self.comp_stream = "W51:S1P:W150:W150:S1P:W100:W200:S1P:";
  }
  
  //(Pair 2)
  with (instance_create(obj_garden_spawn_5.x, obj_garden_spawn_5.y, obj_enemy_spawner)) {
    self.comp_stream = "W49:W150:S1P:W150:W100:S1P:";
  }
  with (instance_create(obj_garden_spawn_3.x, obj_garden_spawn_3.y, obj_enemy_spawner)) {
    self.comp_stream = "W53:W150:S1P:W150:W100:S1P:";
  }
}



//Beam spam! Some ducks, then some more.
if (global.round_id==7) {
  //The beam is pretty standard.
  with (instance_create(obj_beam_spawn_fixed.x, obj_beam_spawn_fixed.y, obj_enemy_spawner)) {
    self.comp_stream = "S1B:";
  }

  //Spawn some ducks after the first beam blast.
  with (instance_create(obj_toy_spawn_3.x, obj_toy_spawn_3.y, obj_enemy_spawner)) {
    self.comp_stream = "W350:D35:S8D:W50:S8D:";
  }
  with (instance_create(obj_toy_spawn_4.x, obj_toy_spawn_4.y, obj_enemy_spawner)) {
    self.comp_stream = "W320:D120:S2M:W50:D35:S8D:";
  }
  
  //Keep a regular stream here too.
  with (instance_create(obj_toy_spawn_2.x, obj_toy_spawn_3.y, obj_enemy_spawner)) {
    self.comp_stream = "W380:D35:S8D:W50:S8D:";
  }
  with (instance_create(obj_toy_spawn_5.x, obj_toy_spawn_4.y, obj_enemy_spawner)) {
    self.comp_stream = "W360:D120:S2M:W50:D35:S8D:";
  }
}


//Beam in a worse spot; LOTs of stuff.
if (global.round_id==8) {
  //The beam is pretty standard.
  with (instance_create(obj_beam_spawn_fixed_2.x, obj_beam_spawn_fixed_2.y, obj_enemy_spawner)) {
    self.comp_stream = "S1B:";
  }
  
  //Tweaked from 5
  with (instance_create(obj_comic_spawn_3.x, obj_comic_spawn_3.y, obj_enemy_spawner)) {
    self.comp_stream = "W5:S1S:W150:S1G:W100:W125:D50:S3S:";
  }
  with (instance_create(obj_comic_spawn_2.x, obj_comic_spawn_2.y, obj_enemy_spawner)) {
    self.comp_stream = "W55:S1S:W100:W40:S1G:W60:W100:D80:S3G:";
  }
  with (instance_create(obj_comic_spawn_4.x, obj_comic_spawn_4.y, obj_enemy_spawner)) {
    self.comp_stream = "W75:S1S:W80:W80:S1G:W20:W85:D50:S3S:";
  }
  
  //Ducks keep you away from the edge
  with (instance_create(obj_toy_spawn_1.x, obj_toy_spawn_1.y, obj_enemy_spawner)) {
    self.comp_stream = "W300:D20:S3D:W5:S1M:W5:S3D:W5:S2M:W5:S3D:";
  }
  with (instance_create(obj_toy_spawn_3.x, obj_toy_spawn_3.y, obj_enemy_spawner)) {
    self.comp_stream = "W350:D20:S3D:W5:S3D:W5:S3D:W5:S6D:W5:S1M:";
  }
  with (instance_create(obj_toy_spawn_5.x, obj_toy_spawn_5.y, obj_enemy_spawner)) {
    self.comp_stream = "W300:D20:S1M:W5:S3D:W5:S3D:W5:S2M:W5:S3D:";
  }
  
  //Plants fire in pairs.
  with (instance_create(obj_garden_spawn_2.x, obj_garden_spawn_2.y, obj_enemy_spawner)) {
    self.comp_stream = "W52:S1P:W150:W150:S1P:W100:W200:S1P:";
  }
  with (instance_create(obj_garden_spawn_4.x, obj_garden_spawn_4.y, obj_enemy_spawner)) {
    self.comp_stream = "W51:S1P:W150:W150:S1P:W100:W200:S1P:";
  }
}


//Super heroes and the beam on the right
if (global.round_id==9) {
  //The beam is pretty standard.
  with (instance_create(obj_beam_spawn_fixed_3.x, obj_beam_spawn_fixed_3.y, obj_enemy_spawner)) {
    self.comp_stream = "S1B:";
  }
  
  //Tweaked from 5
  with (instance_create(obj_comic_spawn_1.x, obj_comic_spawn_3.y, obj_enemy_spawner)) {
    self.comp_stream = "W5:D100:S2S:S2G:S2S:S2G:S2S:S2G:S2S:S2G:S2S:S2G:";
  }
  with (instance_create(obj_comic_spawn_3.x, obj_comic_spawn_2.y, obj_enemy_spawner)) {
    self.comp_stream = "W55:D100:S2G:S2S:S2G:S2S:S2G:S2S:S2G:S2S:S2G:S2S:";
  }
  with (instance_create(obj_comic_spawn_5.x, obj_comic_spawn_4.y, obj_enemy_spawner)) {
    self.comp_stream = "W75:D100:S2S:S2G:S2S:S2G:S2S:S2G:S2S:S2G:S2S:S2G:";
  }
}


//Round 10 is special
if (global.round_id==10) {
  //Spawn the snowman directly (offscreen though).
  instance_create(room_width/2, -200, obj_snowman);

  //We need these eventually to survive
  with (instance_create(obj_comic_spawn_1.x, obj_comic_spawn_3.y, obj_enemy_spawner)) {
    self.comp_stream = "W900:D300:S100S:";
  }
  
}



//Custom level!
if (global.round_id==11) {
  ini_open("custom_level.ini");
  with (instance_create(obj_toy_spawn_1.x, obj_toy_spawn_1.y, obj_enemy_spawner)) {
    self.comp_stream = ini_read_string("Right", "obj_toy_spawn_1", "");
  }
  with (instance_create(obj_toy_spawn_2.x, obj_toy_spawn_2.y, obj_enemy_spawner)) {
    self.comp_stream = ini_read_string("Right", "obj_toy_spawn_2", "");
  }
  with (instance_create(obj_toy_spawn_3.x, obj_toy_spawn_3.y, obj_enemy_spawner)) {
    self.comp_stream = ini_read_string("Right", "obj_toy_spawn_3", "");
  }
  with (instance_create(obj_toy_spawn_4.x, obj_toy_spawn_4.y, obj_enemy_spawner)) {
    self.comp_stream = ini_read_string("Right", "obj_toy_spawn_4", "");
  }
  with (instance_create(obj_toy_spawn_5.x, obj_toy_spawn_5.y, obj_enemy_spawner)) {
    self.comp_stream = ini_read_string("Right", "obj_toy_spawn_5", "");
  }
  
  with (instance_create(obj_garden_spawn_1.x, obj_garden_spawn_1.y, obj_enemy_spawner)) {
    self.comp_stream = ini_read_string("Bottom", "obj_garden_spawn_1", "");
  }
  with (instance_create(obj_garden_spawn_2.x, obj_garden_spawn_2.y, obj_enemy_spawner)) {
    self.comp_stream = ini_read_string("Bottom", "obj_garden_spawn_2", "");
  }
  with (instance_create(obj_garden_spawn_3.x, obj_garden_spawn_3.y, obj_enemy_spawner)) {
    self.comp_stream = ini_read_string("Bottom", "obj_garden_spawn_3", "");
  }
  with (instance_create(obj_garden_spawn_4.x, obj_garden_spawn_4.y, obj_enemy_spawner)) {
    self.comp_stream = ini_read_string("Bottom", "obj_garden_spawn_4", "");
  }
  with (instance_create(obj_garden_spawn_5.x, obj_garden_spawn_5.y, obj_enemy_spawner)) {
    self.comp_stream = ini_read_string("Bottom", "obj_garden_spawn_5", "");
  }
  
  with (instance_create(obj_comic_spawn_1.x, obj_comic_spawn_1.y, obj_enemy_spawner)) {
    self.comp_stream = ini_read_string("Left", "obj_comic_spawn_1", "");
  }
  with (instance_create(obj_comic_spawn_2.x, obj_comic_spawn_2.y, obj_enemy_spawner)) {
    self.comp_stream = ini_read_string("Left", "obj_comic_spawn_2", "");
  }
  with (instance_create(obj_comic_spawn_3.x, obj_comic_spawn_3.y, obj_enemy_spawner)) {
    self.comp_stream = ini_read_string("Left", "obj_comic_spawn_3", "");
  }
  with (instance_create(obj_comic_spawn_4.x, obj_comic_spawn_4.y, obj_enemy_spawner)) {
    self.comp_stream = ini_read_string("Left", "obj_comic_spawn_4", "");
  }
  with (instance_create(obj_comic_spawn_5.x, obj_comic_spawn_5.y, obj_enemy_spawner)) {
    self.comp_stream = ini_read_string("Left", "obj_comic_spawn_5", "");
  }
  
  with (instance_create(obj_beam_spawn_fixed.x, obj_beam_spawn_fixed.y, obj_enemy_spawner)) {
    self.comp_stream = ini_read_string("Top", "obj_beam_spawn_fixed", "");
  }
  with (instance_create(obj_beam_spawn_fixed_2.x, obj_beam_spawn_fixed_2.y, obj_enemy_spawner)) {
    self.comp_stream = ini_read_string("Top", "obj_beam_spawn_fixed_2", "");
  }
  with (instance_create(obj_beam_spawn_fixed_3.x, obj_beam_spawn_fixed_3.y, obj_enemy_spawner)) {
    self.comp_stream = ini_read_string("Top", "obj_beam_spawn_fixed_3", "");
  }

}


































/* comment */