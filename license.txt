Sad little war-bot is copyright 2014 (c) Seth N. Hetu

You are free to use the source code and redistribute the game in any way
as long as you include this copyright file unmodified (or featured prominently 
on an in-game "help" screen).

THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
